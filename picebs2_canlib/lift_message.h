#ifndef XC_HEADER_TEMPLATE_H
#define	XC_HEADER_TEMPLATE_H

#include <xc.h> // include processor files - each processor file is guarded.  

// TODO Insert appropriate #include <>
#include <stdint.h>

// TODO Insert C++ class definitions if appropriate
//Initialize the lift messages
void LifMessage_Init(struct CANFILTER* filtre);

void LifMessage_Init(struct CANFILTER* filtre);

//Request ID of lift on CAN network
void Id_Request();

//Set and clear LEDs in lift
void Led_Lift(uint8_t led);

//Set and clear LEDs on floors
void Led_Floor(uint8_t ledUp, uint8_t ledDown);

//Request all lift's information
void Status_Request();

//Lift reset at position R
void Reset_Lift();

//Manage respones
void Response();

//Receiving status
void status_Response(uint8_t f, uint8_t s, uint8_t d);

//Response when a button in the lift is pressed or released
void btn_Lift(uint8_t lift);

//Response when a button on a floor is pressed or released
void btn_Floor(uint8_t up, uint8_t down);

//Response with the error that caused a crash
void error(uint8_t e);

//Message sent 11 times between each floor
void rail_Lift();

//Logic to control and optimized the lift's behaviour
void logic_Lift(void);
 
// TODO Insert declarations or function prototypes (right here) to leverage 
// live documentation

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* XC_HEADER_TEMPLATE_H */

