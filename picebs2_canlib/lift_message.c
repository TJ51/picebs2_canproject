#include <pic18.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "lift_message.h"
#include "can.h"
#include "door.h"
#include "lightCab.h"
#include "motor.h"

struct CANMESSAGE TXmessage;
struct CANMESSAGE RXmessage;
uint32_t id;

uint8_t posCmd;
uint8_t thinkUp;
uint8_t thinkDown;

uint8_t position;
uint8_t oldChoice;
uint8_t oldCmdMotor;

bool isMove;
bool up;

void LifMessage_Init(struct CANFILTER* filtre) {            //Initialization
    INTCON2bits.INTEDG0 = 1;                                //Interrupt on RB0
    INT0IF = 0;
    INT0IE = 1;

    Can_Init(filtre);
    id = filtre->filter0;                                   //Get the right id
    posCmd = 0;
    thinkUp = 0;
    thinkDown = 0;
    oldChoice = 0;
    oldCmdMotor = 0;
    position = 1;
    isMove = false;
    up = true;
}

//Ask for id
void Id_Request() {
    TXmessage.extended_identifier = 0;
    TXmessage.identifier = 0x7FF;
    TXmessage.dlc = 0;
    TXmessage.dta[0] = 0;
    TXmessage.dta[1] = 0;
    TXmessage.dta[2] = 0;
    TXmessage.dta[3] = 0;
    TXmessage.dta[4] = 0;
    TXmessage.dta[5] = 0;
    TXmessage.dta[6] = 0;
    TXmessage.dta[7] = 0;
    TXmessage.rtr = 0;

    Can_PutMessage(&TXmessage);
}

//Set/clear leds in the cabin
void Led_Lift(uint8_t led) {
    TXmessage.extended_identifier = 0;
    TXmessage.identifier = id + 0x040;
    TXmessage.dlc = 1;
    TXmessage.dta[0] = led;
    TXmessage.dta[1] = 0;
    TXmessage.dta[2] = 0;
    TXmessage.dta[3] = 0;
    TXmessage.dta[4] = 0;
    TXmessage.dta[5] = 0;
    TXmessage.dta[6] = 0;
    TXmessage.dta[7] = 0;
    TXmessage.rtr = 0;


    Can_PutMessage(&TXmessage);

}

//Set/clear leds on floors
void Led_Floor(uint8_t ledUp, uint8_t ledDown) {
    TXmessage.extended_identifier = 0;
    TXmessage.identifier = id + 0x060;
    TXmessage.dlc = 2;
    TXmessage.dta[0] = ledUp;
    TXmessage.dta[1] = ledDown;
    TXmessage.dta[2] = 0;
    TXmessage.dta[3] = 0;
    TXmessage.dta[4] = 0;
    TXmessage.dta[5] = 0;
    TXmessage.dta[6] = 0;
    TXmessage.dta[7] = 0;
    TXmessage.rtr = 0;

    Can_PutMessage(&TXmessage);
}

//Ask for lift' status
void Status_Request() {
    TXmessage.extended_identifier = 0;
    TXmessage.identifier = id + 0x0A0;
    TXmessage.dlc = 0;
    TXmessage.dta[0] = 0;
    TXmessage.dta[1] = 0;
    TXmessage.dta[2] = 0;
    TXmessage.dta[3] = 0;
    TXmessage.dta[4] = 0;
    TXmessage.dta[5] = 0;
    TXmessage.dta[6] = 0;
    TXmessage.dta[7] = 0;
    TXmessage.rtr = 0;

    Can_PutMessage(&TXmessage);
}

//Reset
void Reset_Lift() {
    TXmessage.extended_identifier = 0;
    TXmessage.identifier = id + 0x0C0;
    TXmessage.dlc = 0;
    TXmessage.dta[0] = 0;
    TXmessage.dta[1] = 0;
    TXmessage.dta[2] = 0;
    TXmessage.dta[3] = 0;
    TXmessage.dta[4] = 0;
    TXmessage.dta[5] = 0;
    TXmessage.dta[6] = 0;
    TXmessage.dta[7] = 0;
    TXmessage.rtr = 0;

    Can_PutMessage(&TXmessage);
}

void Response() {
    if (Can_GetMessage(&RXmessage) == 0) {
        switch (RXmessage.identifier & 0b111111110000) {
            case 0x100: btn_Lift(RXmessage.dta[0]);
                break;
            case 0x120: btn_Floor(RXmessage.dta[0], RXmessage.dta[1]);
                break;
            case 0x0A0: status_Response(RXmessage.dta[0], RXmessage.dta[1], RXmessage.dta[2]);
                break;
            case 0x140: error(RXmessage.dta[0]);
                break;
            case 0x300: rail_Lift();
                break;
            default:
                break;
        }
    }
}

void error(uint8_t e)
{
    LATH = e;
}

//Response to a status request
void status_Response(uint8_t f, uint8_t s, uint8_t d) {
    position = f;                           //current floor
    Motor_MovePos(position);                //send the current floor to motor

    if (f == 0b10000000)                    //on last floor
        up = false;
    if (f == 0b00000001)                    //on ground floor
        up = true;

    Door_SetState(d);                       //update door's state

    if (isMove && s == 0 && (position & (posCmd | thinkUp | thinkDown)) != 0) {     //Arrived to destination
        Door_Open();
        LATH = !position;
        posCmd = posCmd & (0xFF ^ position);                                        //Remove the current floor from positions to reach
        thinkUp = thinkUp & (0xFF ^ position);
        thinkDown = thinkDown & (0xFF ^ position);
        Led_Lift(posCmd);                                                           //Update the leds in lift and floors
        Led_Floor(thinkUp, thinkDown);
        isMove = false;
    } else if (isMove && d == 0 && s == 0 && (position & (posCmd | thinkUp | thinkDown)) == 0) {    //Not a destination
        isMove = false;
    }
}

//A button is pressed/released in the cabin
void btn_Lift(uint8_t lift) {
    LightCab_On();                                          //Turn on light

    posCmd = posCmd | lift;                                 //Add the new destination
    Led_Lift(posCmd);                                       //Update lift's leds

    if (lift == position && !Motor_IsMove()) {              //On the same floor
        Door_Open();
        posCmd = posCmd & (0xFF ^ position);                //Remove the current floor from positions to reach
        thinkUp = thinkUp & (0xFF ^ position);
        thinkDown = thinkDown & (0xFF ^ position);
        Led_Lift(posCmd);                                   //Update the leds in lift and floors
        Led_Floor(thinkUp, thinkDown);
    }
}

//A button is pressed/released on a floor
void btn_Floor(uint8_t up, uint8_t down) {
    thinkUp = thinkUp | up;                                 //Add the new upward destination
    thinkDown = thinkDown | down;                           //Add the new downward destination
    Led_Floor(thinkUp, thinkDown);                          //Update floors' leds

    if (up == position && !Motor_IsMove()) {                //Is on the floor
        Door_Open();
        posCmd = posCmd & (0xFF ^ position);
        thinkUp = thinkUp & (0xFF ^ position);
        thinkDown = thinkDown & (0xFF ^ position);
        Led_Lift(posCmd);
        Led_Floor(thinkUp, thinkDown);
    }
    if (down == position && !Motor_IsMove()) {              //Is on the floor
        Door_Open();
        posCmd = posCmd & (0xFF ^ position);
        thinkUp = thinkUp & (0xFF ^ position);
        thinkDown = thinkDown & (0xFF ^ position);
        Led_Lift(posCmd);
        Led_Floor(thinkUp, thinkDown);
    }
}

//Send 11 message between each floor
void rail_Lift() {
    Motor_Move();
}

//Lift's behaviour
void logic_Lift() {
    
    if (posCmd != 0){       //There still floors to reach
        LightCab_On();
    }
    
    uint8_t choice = posCmd | thinkDown | thinkUp;  //List of floors to reach

    if (choice != 0 && !isMove) {                   //Not done with floors
        oldChoice = choice;

        uint8_t i = position;
        uint8_t c = choice;
        for (; i != 0b00000000;) {
            i = i << 1;
            c = c << 1;
        }
        if (c == 0 && !up) {                        //Can go up
            up = true;
        }
        if (position > choice && up) {              //Must go down
            up = false;
        }

        if (up) {
            choice = posCmd | thinkUp;
            for (uint8_t i = position; i < 0b10000000 && !isMove; i = i << 1) {
                if ((i & choice) != 0) {                //There is still somewhere to go upward
                    oldCmdMotor = (i & choice);         //Next stop
                    Motor_MoveCmd(oldCmdMotor, position);   //Go to next stop
                    isMove = true;
                }
            }
            if ((0b10000000 & choice) != 0 && !isMove) {    //Go to last floor
                oldCmdMotor = (0b10000000 & choice);
                Motor_MoveCmd(oldCmdMotor, position);
                isMove = true;
            }
            choice = choice | thinkDown;
            for (uint8_t i = 0b10000000; i > position && !isMove; i = i >> 1) {
                if ((i & choice) != 0) { 
                    oldCmdMotor = (i & choice);
                    Motor_MoveCmd(oldCmdMotor, position);
                    isMove = true;
                }
            }
        }
        if (!up) {
            choice = posCmd | thinkDown;
            for (uint8_t i = position; i > 0b00000001 && !isMove; i = i >> 1) {
                if ((i & choice) != 0) {               //There is still somewhere to go downward
                    oldCmdMotor = (i & choice);         //Next stop
                    Motor_MoveCmd(oldCmdMotor, position);   //Go to next stop
                    isMove = true;
                }
            }
            if ((0b00000001 & choice) != 0 && !isMove) {    //Go to ground floor
                oldCmdMotor = (0b00000001 & choice);
                Motor_MoveCmd(oldCmdMotor, position);
                isMove = true;
            }
            choice = choice | thinkUp;
            for (uint8_t i = 0b00000001; i < position && !isMove; i = i << 1) {
                if ((i & choice) != 0) {
                    oldCmdMotor = (i & choice);
                    Motor_MoveCmd(oldCmdMotor, position);
                    isMove = true;
                }
            }
        }

    } else if (choice != oldChoice) {           //A new command
        if (up) {
            choice = posCmd | thinkUp | (thinkUp & thinkDown);      //Update choice
        }
        if (!up) {
            choice = posCmd | thinkDown | (thinkUp & thinkDown);    //Update choice
        }

        if (up && ((choice ^ oldChoice) > (position << 2) && (choice ^ oldChoice) < oldCmdMotor)) { //New choice is next stop  on the way up
            oldCmdMotor = (choice ^ oldChoice);
            Motor_MoveCmd(oldCmdMotor, position);
        }
        if (!up && ((choice ^ oldChoice) < (position >> 2) && (choice ^ oldChoice) > oldCmdMotor)) {//New choice is next stop on the way down
            oldCmdMotor = (choice ^ oldChoice);
            Motor_MoveCmd(oldCmdMotor, position);
        }
        oldChoice = choice;
    }

}