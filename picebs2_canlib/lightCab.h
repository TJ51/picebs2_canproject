/* 
 * File:   lightCab.h
 * Author: Mari�thoz C�dric
 *
 * Created on 10. juin 2020, 09:20
 */

#ifndef LIGHTCAB_H
#define	LIGHTCAB_H

/*
 * Init LightCab
 * set default value
 * @param struct CANFILTER pointer use for address of lift
 */
void LightCab_Init(struct CANFILTER* filtre);

/*
 * This method turns on the lamp in the lift
 * and stop timer 1
 */
void LightCab_On(void);

/*
 * This method turns off the lamp in the lift
 * and stop timer 1
 */
void LightCab_Off(void);

/*
 * This method strat timer 1
 */
void LightCab_AutoOff(void);

/*
 * This method send standart message for lamp in the lift
 * @param state; 0 = lamp off,   1 = lamp on
 */
void LightCab_Send(uint8_t state);

#endif	/* LIGHTCAB_H */

