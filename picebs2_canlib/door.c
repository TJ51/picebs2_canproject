
#define _XTAL_FREQ 64000000L

#include <pic18.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "door.h"
#include "can.h"
#include "motor.h"
#include "lightCab.h"

uint8_t doorState;  //< 0=close, 3=open
uint32_t id;        //can address of lift
uint8_t doorTime;   //Is number of times the timer is activated befor door close


void Door_Init(struct CANFILTER* filtre)
{
    Can_Init(filtre);
    doorState = 0;
    doorTime = 0;
    id = filtre->filter0;
    
    //Init TIMER 3
    TMR3ON = 1;             
    TMR3IE = 0;                 //disable interupt
    TMR3 = 0;
    T3CONbits.SOSCEN = 1;       //f timer 32768Hz
    T3CONbits.TMR3CS = 0b10;    //extern clock
    T3CONbits.T3CKPS = 0b00;    //prescal = 1
    //timer -> 2s
    //time = doorTime * 2
}

bool Door_IsClose(void)
{
    switch(doorState)
    {
        case 0:
            return true;
        default:
            return false;
    }
}

void Door_Open(void)
{
    if(!Motor_IsMove())
    {
        doorState = 1;
        Door_Send(1);
        Door_AutoCloseOn();
        LightCab_On();
    }
}

void Door_Close(void)
{
    TMR3IE = 0;
    doorTime = 5;
    TMR3 = 0;
    Door_Send(0);
    LightCab_AutoOff();    
}

void Door_AutoCloseOn(void)
{
    doorTime = 5;
    TMR3 = 0;
    TMR3IE = 1;
}

void Door_SetState(uint8_t state)
{
    doorState = state;
}

void Door_Send(uint8_t state)
{
    struct CANMESSAGE doorMessage;
    
    doorMessage.rtr = 0;
    doorMessage.identifier = id + 0x080;
    doorMessage.extended_identifier = 0;
    
    doorMessage.dlc = 1;    
    doorMessage.dta[0] = state;
    doorMessage.dta[1] = 0;
    doorMessage.dta[2] = 0;
    doorMessage.dta[3] = 0;
    doorMessage.dta[4] = 0;
    doorMessage.dta[5] = 0;
    doorMessage.dta[6] = 0;
    doorMessage.dta[7] = 0;
    
    Can_PutMessage(&doorMessage);       
    
}

