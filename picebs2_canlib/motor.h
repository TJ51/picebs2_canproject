/* 
 * File:   motor.h
 * Author: Mari�thoz C�dric
 *
 * Created on 29. mai 2020, 13:00
 */

#ifndef MOTOR_H
#define	MOTOR_H

#include "stdbool.h"


/*
 * Init Motor value and standard message for motor control
 * @param struct CANFILTER pointer use for address of lift
 */
void Motor_Init(struct CANFILTER* filtre);

/*
 * Accelerates or decelerates the motorore according to the desired movement
 */
void Motor_Move(void);

/*
 * Motor is in move
 */
bool Motor_IsMove(void);

/*
 * Set Motor move
 * @param orcmd is the floor we want to go to
 * @param pos is the floor we are on
 */
void Motor_MoveCmd(uint8_t orcmd, uint8_t pos);

/*
 * Control Motor move with position of lift
 * @param pos is the floor we are on
 */
void Motor_MovePos(uint8_t pos);

/*
 * Motor move up
 */
void Motor_Up(void);

/*
 * Motor move down
 */
void Motor_Down(void);

/*
 * Motor stop
 */
void Motor_Stop(void);

/*
 * Send the standard message for command Motor
 */
void Motor_Send(void);

#endif	/* MOTOR_H */

