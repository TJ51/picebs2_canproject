/* 
 * File:   door.h
 * Author: Mari�thoz C�dric
 *
 * Created on 7. juin 2020, 14:13
 */

#ifndef DOOR_H
#define	DOOR_H


/*
 * Init Door value and standard message for door control
 * @param struct CANFILTER pointer use for address of lift
 */
void Door_Init(struct CANFILTER* filtre);

/*
 * Door is close
 */
bool Door_IsClose(void);

/*
 * Open the door of the lift
 */
void Door_Open(void);

/*
 * Close the door of the lift
 * and stop timer 3
 */
void Door_Close(void);

/*
 * Start the timer 3 for close door
 */
void Door_AutoCloseOn(void);

/*
 * Set state of door
 * @param state; 0 = door close,   other = door open or openning or closing
 */
void Door_SetState(uint8_t state);

/*
 * Send standart can message for door command
 * @param state; 0 = door will close,   1 = door will open
 */
void Door_Send(uint8_t state);

#endif	/* DOOR_H */

