
#define _XTAL_FREQ 64000000L

#include <pic18.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "door.h"
#include "can.h"
#include "motor.h"
#include "lightCab.h"

uint32_t id;         //Is can address of lift
uint8_t lightCabTime;//Is number of times the timer is activated befor lamp off


void LightCab_Init(struct CANFILTER* filtre)
{    
    Can_Init(filtre);
    lightCabTime = 5;
    id = filtre->filter0;
    
    //Init TIMER 1
    TMR1ON = 1;             
    TMR1IE = 0;                 //disable interupt
    TMR1 = 0;
    T1CONbits.SOSCEN = 1;       //f timer 32768Hz
    T1CONbits.TMR1CS = 0b10;    //extern clock
    T1CONbits.T1CKPS = 0b00;    //prescal = 1
    //timer -> 2s
    //time = lightCabTime * 2
}

void LightCab_On(void)
{
    //if lightCabTime = 0; the light is already on
    if(lightCabTime != 0){
        TMR1IE = 0;
        lightCabTime = 0;
        TMR1 = 0;
        LightCab_Send(1);
    }
}

void LightCab_Off(void)
{
    TMR1IE = 0;
    lightCabTime = 5;
    TMR1 = 0;
    LightCab_Send(0);
}

void LightCab_AutoOff(void)
{
    lightCabTime = 5;
    TMR1 = 0;
    TMR1IE = 1;
}

void LightCab_Send(uint8_t state)
{
    struct CANMESSAGE lichtCabMessage;
    
    lichtCabMessage.rtr = 0;
    lichtCabMessage.identifier = id + 0x0E0;
    lichtCabMessage.extended_identifier = 0;
    
    lichtCabMessage.dlc = 1;    
    lichtCabMessage.dta[0] = state;
    lichtCabMessage.dta[1] = 0;
    lichtCabMessage.dta[2] = 0;
    lichtCabMessage.dta[3] = 0;
    lichtCabMessage.dta[4] = 0;
    lichtCabMessage.dta[5] = 0;
    lichtCabMessage.dta[6] = 0;
    lichtCabMessage.dta[7] = 0;
    
    Can_PutMessage(&lichtCabMessage);       
    
}