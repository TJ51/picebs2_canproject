
#include <pic18.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "door.h"
#include "motor.h"
#include "can.h"

static int8_t maxSpeed = 110;   //Vitesse Max
static int8_t maxStep = 10;     //Speed step

int8_t speed;       //vitesse
int8_t move;        //< 0=stop, 1=up, 2=down, other=stop
uint8_t cmd;        //floor we will go
uint8_t prcmd;      //floor befor 
bool cmdMove;       //run only one command
uint32_t id;        //can address of lift

void Motor_Init(struct CANFILTER* filtre)
{
    Can_Init(filtre);
    speed = 0;
    move = 0;
    cmd = 0;
    prcmd = 0;
    cmdMove = false;
    id = filtre->filter0;
}

void Motor_Move(void)
{
    switch(move)
    {
        case 1:
            Motor_Up();
            break;
        case 2:
            Motor_Down();
            break;
        default:
            Motor_Stop();
            break;
    }
}

bool Motor_IsMove(void)
{
    return cmdMove;
}


void Motor_MoveCmd(uint8_t orcmd, uint8_t pos)
{
    if(Door_IsClose()){
        if (orcmd > pos)
        {
            cmdMove = true;
            prcmd = orcmd >> 1;
            Motor_Up();
        }else if (orcmd < pos)
        {
            cmdMove = true;
            prcmd = orcmd << 1;
            Motor_Down();
        }else
        {
            move = 0;
        }
        if(prcmd == pos)
        {
            switch(move)
            {
                case 1:
                    move = 2;
                    break;
                case 2:
                    move = 1;
                    break;
                default:
                    move = 0;
                    break;
            }
        }
        
        cmd = orcmd;
    }
}

void Motor_MovePos(uint8_t pos)
{
    if(pos == prcmd)
    {
        switch(move)
        {
            case 1:
                move = 2;
                break;
            case 2:
                move = 1;
                break;
            default:
                move = 0;
                break;
        }
    }else if (pos == cmd){
        Motor_Stop();
    }
    Motor_Move();
}

void Motor_Up(void)
{
    move = 1;
    if(speed < maxSpeed && speed+maxStep != 0)
    {
        speed += maxStep;
        Motor_Send();
    }    
}

void Motor_Down(void)
{
    move = 2;
    if(speed > -maxSpeed && speed-maxStep != 0)
    {
        speed -= maxStep;
        Motor_Send();
    }    
}

void Motor_Stop(void)
{
    move = 0;
    if(speed > 0)
    {
        speed -= maxStep;
        Motor_Send();
    }
    else if(speed < 0)
    {
        speed += maxStep;
        Motor_Send();
    }
    else if(cmdMove)
    {
        speed = 0;
        Motor_Send();
        cmdMove = false;
    }
}


void Motor_Send(void)
{
    
    struct CANMESSAGE motorMessage; //Standart message for command motor

    motorMessage.rtr = 0;
    motorMessage.extended_identifier = 0;
    motorMessage.identifier = 0x020 + id;
    
    motorMessage.dlc = 1;    
    motorMessage.dta[0] = speed;
    motorMessage.dta[1] = 0;
    motorMessage.dta[2] = 0;
    motorMessage.dta[3] = 0;
    motorMessage.dta[4] = 0;
    motorMessage.dta[5] = 0;
    motorMessage.dta[6] = 0;
    motorMessage.dta[7] = 0;
    
    Can_PutMessage(&motorMessage);   
}