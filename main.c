/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>        /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>       /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>   /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#endif
#define _XTAL_FREQ 64000000L

#include "picebs2_canlib/can.h"
#include "picebs2_canlib/lift_message.h"
#include "picebs2_lcdlib/lcd_highlevel.h"
#include "picebs2_canlib/door.h"
#include "picebs2_canlib/motor.h"
#include "picebs2_canlib/lightCab.h"



/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/


/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/
    
    struct CANMESSAGE message;
    struct CANFILTER filtre;

void main(void)
{
    /* Configure the oscillator for the device works @ 64MHz*/
   PLLEN = 1;            // activate PLL x4
   OSCCON = 0b01110000;  // for 64MHz cpu clock (default is 8MHz)
    // Caution -> the PLL needs up to 2 [ms] to start !
    __delay_ms(2); 
    /* Initialize I/O and Peripherals for application */

    //Enable interrupts
    GIE = 1;
    PEIE = 1;
    
    filtre.ext0 = 0;
    filtre.ext1 = 0;
    filtre.mask0 = 0;
    
    TRISH = 0;
    LATH = 0;
    
    LifMessage_Init(&filtre);
        
    //ID_REQUEST MESSAGE
    Id_Request();
    
    bool init = true;
    while(init)             //Wait for id response
    {
        if (Can_GetMessage(&message) == 0){
            switch(message.identifier & 0b11111100000){
                case 0x7E0:
                    filtre.mask0 = 0x01F;
                    filtre.filter0 = message.identifier - 0x7E0;
                    filtre.filter1 = message.identifier & 0x01F;
                    init = false;
                    break;
                default:
                    break;
            }
        }
    }
    
    //Initialize all lift components
    LifMessage_Init(&filtre);
    Can_Init(&filtre); 
    Motor_Init(&filtre);
    LightCab_Init(&filtre);
    Door_Init(&filtre);
             
    while(1)
    {
        logic_Lift();
        __delay_ms(5);
        if (Can_GetMessage(&message) == 0){
            switch(message.identifier & 0b11111100000){
                case 0x0A0:     //Status answer
                    status_Response(message.dta[0], message.dta[1], message.dta[2]);
                    break;
                case 0x100:     //Btn in left
                    if(message.dta[1] == 1)
                    {
                        btn_Lift(message.dta[0]);
                    }
                    break;
                case 0x120:     //Btn in floor
                    if(message.dta[2] == 1)
                    {
                        btn_Floor(message.dta[0], message.dta[1]);
                    }
                    break;
                case 0x140:     //Error
                    break;
                case 0x300:     //Rail lift
                    rail_Lift();
                    break;
                default:
                    break;
            }
        }
    }
}
