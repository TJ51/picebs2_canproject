/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#elif defined(__18CXX)
    #include <p18cxxx.h>    /* C18 General Include File */
#endif

#if defined(__XC) || defined(HI_TECH_C)

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include "picebs2_canlib/can.h"
#include "picebs2_canlib/lift_message.h"
#include "picebs2_canlib/door.h"
#include "picebs2_canlib/lightCab.h"
#endif
 
/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/
/* High-priority service */

void interrupt high_isr(void)
{
  if((CAN_INTF == 1)&&(CAN_INTE == 1)) // interrupt flag & active
  {
    CAN_INTF = 0;               // clear interrupt
    Can_Isr();                  // interrupt treatment
    if(CAN_INTPIN == 0)         // check pin is high again
    {
      CAN_INTF = 1;             // no -> re-create interrupt
    }
  }
  
  if(INT0IF == 1)               //reset lift bouton
  {
<<<<<<< interrupts.c
      Reset_Lift();             //Sends a message to the lift for reset
      INT0IF = 0;               //clear
=======
      Reset_Lift();
      INT0IF = 0;
>>>>>>> interrupts.c
  }

  if(TMR3IF == 1 && TMR3IE == 1)    //interrupt for door close
  {     
      extern uint8_t doorTime;      //intern at door.c
      TMR3IF = 0;
      if(doorTime > 0)              //Shuts the door at the end of a specified time
      {
          doorTime--;
          if(doorTime == 0)
          {
              Door_Close();
          }
      }
  }
  
  if(TMR1IF == 1 && TMR1IE == 1)    //interrupt for light cab off
  {     
      extern uint8_t lightCabTime;      //intern at door.c
      TMR1IF = 0;
      if(lightCabTime > 0)          //Turn off the light in the lift at the end
      {                             //of a specified time.
          lightCabTime--;
          if(lightCabTime == 0)
          {
              LightCab_Off();
          }
      }
  }
}
